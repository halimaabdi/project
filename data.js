import React, { useState, useEffect } from 'react'


function Users(props) {
    return <li>{props.value}</li>;}

function App() {
    const [username, setMessage] = useState([])
  
    useEffect(() => {
      fetch("/users")
        .then((res) => res.json())
        .then((data) => {
          setMessage(data.username);
        });
    }, []);

    const users = username.map((name) =>
    
      <Users key={name.toString()} value={name} />  );
    return (
      <table>
      <thead>
        <tr>
          <th>Usernames</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>{users}</td>
        </tr>
      </tbody>
    </table>
     
    );
  }

export default App