import React from 'react'
import { Link } from 'react-router-dom'

export default function form() {
    return (
        <div className="text-center m-5-auto">
            <h2>Enter project ID</h2>
            <form action="/form">
                <p>
                    <label>project_id</label><br/>
                    <input type="text" name="Project ID" required />
                </p>
                <p>
                    <Link to="/data"><button>
                        Submit
                    </button>
                    </Link>
                </p>
            </form>
         
        </div>
    )
}